<table class="table">
        <thead>
                <tr>
                <th scope="col">Name</th>
                <th scope="col">Quantity</th>
                <th scope="col">Category</th>
                </tr>
            </thead>  
            <tbody>         
            <tr>
            @for($i=0;$i<count($ingredients);$i++)
                <td>{{$ingredients[$i]->name}}</td>
                <td>{{$ingredients[$i]->quantity}}</td>
                @for ($j=0;$j<count($ingredients[$i]->categories);$j++)
                    <td>
                    {{$ingredients[$i]->categories[$j]->name}}
                    </td>
                @endfor 
                <td> <a href="{{ route('ingredients.edit', $ingredients[$i]) }}" class="btn btn-primary">Edit</a> </td>
                <td>
                    <form method="POST" action="{{ route('ingredients.delete', $ingredients[$i]) }}">
                    @csrf 
                    @method('DELETE')
                    <input type="submit" value="Delete" class="btn btn-danger">  
                    </form> 
                </td> 
            </tr>
            @endfor
         <tbody>
    </table>
