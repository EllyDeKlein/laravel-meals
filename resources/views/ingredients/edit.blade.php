@extends ('layouts/app')
@section ('content')
<h1> Edit Ingredient </h1>
<hr> <p>Waarom kan ik ingredients niet ingredient noemen? </p>
<h2> Modify name/quantity </h2>
<div id='cssform'>
    <h1>Artikel aanpassen</h1>
        <form method ="POST" action="/ingredients/{{$ingredients->id}}/">  
        @csrf 
        @method('PUT')
        <div class="field">
            <label class="label" for="name">Name</label>
        <div class ="control">
             <input class="input" type="text" name="name" id="name" value="{{$ingredients->name}}">
        </div>
        <div class="field">
            <label class="label" for="content">Content</label>
        <div class ="control">
        <input class="input" type="number" name="quantity" id="quantity" value="{{$ingredients->quantity}}">
        </div>
        </div>
        <div class="field">
            <label class="label" for="content">Categories</label>
        <div class ="control">
            <select 
            name="categories[]"
            multiple
            >
            @foreach ($categories as $category)
            <option value="{{$category->id}}">{{ $category->name}}</option>
            @endforeach
            </select>
                @if ($errors->has('categories'))
                    <p>{{$errors->first('categories')}}</p>
                @endif 
        </div>
        <button type="submit">Submit</button>
    </form>
</div>
<a href="{{ route('meals.ingredients')}}">Back to ingredients</a>
@endsection
