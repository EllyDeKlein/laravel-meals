@extends ('layouts/app')
@section ('content')
<div id='cssform'>
    <h1>Add Ingredient</h1>
        <form method ="POST" action="/ingredients/store">  
        @csrf 
        <div class="field">
                <label class="label" for="name">Name</label>
            <div class ="control">
                <input class="input" type="text" name="name" id="name" value="">
            </div>
            <div class="field">
                <label class="label" for="content">Quantity</label>
            <div class ="control">
                <input class="input" type="number" name="quantity" id="quantity" value="">
            </div>

            <div class="field">
            <label class="label" for="content">Categories</label>
        <div class ="control">
            <select 
            name="categories[]"
            multiple
            >
            @foreach ($categories as $category)
            <option value="{{$category->id}}">{{ $category->name}}</option>
            @endforeach
            </select>
                @if ($errors->has('categories'))
                    <p>{{$errors->first('categories')}}</p>
                @endif 
        </div>
        <button type="submit">Submit</button>
    </form>
</div>
<a href="{{ route('meals.home')}}">Back to home</a>
@endsection
