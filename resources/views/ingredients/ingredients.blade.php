@extends ('layouts/app')
@section ('content')
    <h1> Modify or add ingredients </h1>
        <img src="{{ asset('images/blackapple.png') }}" alt=""  width="50" height="50" id="ingredientsimg">
        <br>
        <br>
        <a href="{{ route('ingredients.create')}}" class="btn btn-success">Add ingredient</a>
        <br><br>
        <div>
            <h1> Meal Suggestions for you!!! </h1>
            @for($i=0;$i<count($meals);$i++)
             <ul>
                <li>{{$meals[$i]->name}}</li>
            </ul>
            @endfor
        </div>
            <p> Filter by category </p>
        <div class ="control">
                <select 
                id="categories"
                name="categories[]"
                multiple
                >
                @foreach ($categories as $category)
                <option onclick="getIngredients()" value="{{ $category->id}}">{{ $category->name}}</option>
                @endforeach
                </select>
        <div id="ingredientContainer">
            @include('ingredients.render')
        </div>
@endsection
