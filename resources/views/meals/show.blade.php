@extends ('layouts/app')
@section ('content')
    <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="{{ asset('images/showcard.jpg') }}" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title">{{$meal->name}}</h5>
            <p>Ingredients</p>
            @foreach($meal->ingredients as $ingredient)
                <ul>
                    <li>{{$ingredient->name}}</li>
                </ul>
            @endforeach
            <p class="card-text">{{$meal->recipe}}</p>
            <a href="#" class="btn btn-primary">Edit</a>
        </div>
    </div>
@endsection
