@extends ('layouts/app')
@section ('content')
    <h1>Edit a meal</h1>
    <hr>
    <form method ="POST" action="/meals/update/{{$meal->id}}/" enctype="multipart/form-data" id="ingredients_form">
        @csrf
        @method('PUT')
        <div class="container">

            <div class="row">
                <div class="col-10">
                    <p class="text-primary">Add Ingredients for your menu</p>
                </div>
                <div class="col-2">
                    <input name="button_add" id="button_add" value="+" class="btn btn-success btn-block ingredient-add" type="button" />
                </div>
            </div>

            <div id="ingredients">
                @foreach ($meal->ingredients as $chosen_ingredient)
                    <div class="row mt-2">
                        <input type="hidden" value="{{$chosen_ingredient->pivot->id}}" name="row[]" />
                        <div class="col-5">
                            <select name="ingredient[]">
                                @foreach ($ingredients as $ingredient)
                                    @if($ingredient->id == $chosen_ingredient->id)
                                        <option value="{{$ingredient->id}}" selected="selected">{{$ingredient->name}}</option>
                                    @else
                                        <option value="{{$ingredient->id}}">{{$ingredient->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-5">
                            <input type="number" value="{{$chosen_ingredient->pivot->qty_required}}" name="amount[]" />
                        </div>
                        <div class="col-2">
                            <input name="button_edit[]" value="-" class="btn btn-danger btn-block ingredient-remove" type="button" />
                        </div>
                    </div>
                @endforeach
            </div>
            
            <div class="row">
                <div class="col-12">
                    <p class="text-primary">Meal name:</p>
                    <input class="input" type="text" name="name" id="name" value="{{$meal->name}}" required />
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <p class="text-primary">Recipe description:</p>
                    <textarea id="recipe" name="recipe" rows="4" cols="50" value="recipe">{{$meal->recipe}}</textarea>
                </div>
            </div>
            
            <div class="row">
                <div class="col-12">
                    <button type="submit" class="btn-block btn-primary">Update</button>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(function ()
        {
            $(document).on("click", ".ingredient-remove" ,function(e){
                e.preventDefault();
                var row = $(this).parent().parent();
                row.remove();
            });

            $('.ingredient-add').click(function(e)
            {
                e.preventDefault();
                $('#ingredients_form').find('#ingredients').append('<div class="row mt-2"><input type="hidden" value="0" name="row[]" /><div class="col-5"><select name="ingredient[]">@foreach ($ingredients as $ingredient)<option value="{{$ingredient->id}}">{{$ingredient->name}}</option>@endforeach</select></div><div class="col-5"><input type="number" value="0" name="amount[]" /></div><div class="col-2"><input name="button_edit[]" value="-" class="btn btn-danger btn-block ingredient-remove" type="button" /></div></div>');
            });
        });
    </script>
@endsection
