@extends ('layouts/app')
@section ('content')
    <h1 class="text-center"> Overview meals </h1>
    <img src="{{ asset('images/food.svg') }}" alt="" class="rounded mx-auto d-block" width="200" height="200" id="welcomeimg">
    <hr>
    <div class="col-sm">
        <h1> Top 5 Meals </h1>
        @for($i=0;$i<count($topMeals);$i++)
            <ul> 
                <li>{{$topMeals[$i]->name}}</li>
            </ul>
        @endfor
    </div>
    <hr>
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
<table class="table">
    <thead>
            <tr>
            <th scope="col">Name</th>
            </tr>
        </thead>           
        <tr>
        @for($i=0;$i<count($meals);$i++)
            <td>{{$meals[$i]->name}}</td>
            <td> <a href="{{ route('meal.show', $meals[$i]) }}" class="btn btn-success">View Recipe</a> </td>
            <td> <a href="{{ route('meal.selected', $meals[$i]) }}" class="btn btn-warning">Make this meal</a> </td>
            <td>
                <form method="POST" action="{{ route('meals.delete', $meals[$i]) }}">
                    @csrf
                    @method('DELETE')
                <input type="submit" value="Delete" class="btn btn-danger">   
                </form>
            </td>
        </tr>
        @endfor
</table>

@endsection