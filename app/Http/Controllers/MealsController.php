<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Meal;
use App\Models\Ingredient;
use App\Models\IngredientsMealRel;

class MealsController extends Controller
{
  
    public function index()
    {
        $data['meals'] = Meal::all();
        $data['topMeals'] = Meal::get()->sortByDesc('counter')->take(5);
        return view('welcome', $data);
    }

  
    public function create()
    {
        $data['ingredients'] = Ingredient::all();  
        return view('meals.meals', $data);
    }

  
    public function store(Request $request)
    {
        $meal           = new Meal; 
        $meal->name     = $request->name;
        $meal->recipe   = $request->recipe;
        $meal->save();

        $ingredient     = $request->ingredient;
        $amount         = $request->amount;

        for($i=0; $i < count($ingredient); $i++)
        {
            $ingredientmeal                 = new IngredientsMealRel;
            $ingredientmeal->meal_id        = $meal->id;
            $ingredientmeal->ingredient_id  = $ingredient[$i];
            $ingredientmeal->qty_required       = $amount[$i];
            $ingredientmeal->save();

        }


            
        return redirect(route('meals.home'));
    }

  
    public function show(Meal $meal)
    {
        $data['meal'] = $meal;
        return view('meals.show', $data);
    }

  
    public function edit(Meal $meal, $id)
    {
        $data['meal'] = Meal::find($id);
        $data['ingredients'] = Ingredient::all();

        return view('meals.edit', $data);
    }

   
    public function update(Request $request, $id)
    {
        $meal           = Meal::find($id);
        $meal->name     = $request->name;
        $meal->recipe   = $request->recipe;
        $meal->save();

        $row            = $request->row;
        $ingredient     = $request->ingredient;
        $amount         = $request->amount;
        $is_not_delete  = array();
     
        if(!empty($row))
        {
            for($i=0; $i < count($row); $i++)
            {
                if(!$row[$i]==0)
                {
                    $ingredientmeal                 = IngredientsMealRel::find($row[$i]);
                    $ingredientmeal->meal_id        = $meal->id;
                    $ingredientmeal->ingredient_id  = $ingredient[$i];
                    $ingredientmeal->qty_required   = $amount[$i];
                    $ingredientmeal->save();
                    $is_not_delete[]                = $ingredientmeal->id;
                    unset($ingredientmeal);
                }
                else
                {
                    
                    $ingredientmeal                 = new IngredientsMealRel;
                    $ingredientmeal->meal_id        = $meal->id;
                    $ingredientmeal->ingredient_id  = $ingredient[$i];
                    $ingredientmeal->qty_required   = $amount[$i];
                    $ingredientmeal->save();
                    $is_not_delete[]                = $ingredientmeal->id;
                    
                    unset($ingredientmeal);
                }
            }
        }
        if(!empty($is_not_delete))
        {
            IngredientsMealRel::where('meal_id','=', $meal->id)->whereNotIn('id', $is_not_delete)->delete();
        }
        else
        {
            IngredientsMealRel::where('meal_id','=', $meal->id)->delete();
        }
        unset($is_not_delete);

        return redirect()->back()->with('message', 'Enjoy your Meal! Updated your inventory!');
    }

    public function destroy(Meal $meals)
    {
        $meals->delete();
        return redirect(route('meals.home'));
    }

    public function validateMeal() {
        return request()->validate([
            'name' => ['required', 'min:2'],
            'recipe' => ['required', 'min:10']
        ]);
    }
    
    public function updateInventory($id) {  
      
            $meal = Meal::find($id);
            $meal->increment('counter'); 

        foreach ($meal->ingredients as $ingredient) {
            if( $ingredient->quantity >= $ingredient->pivot->qty_required) {
                $value = $ingredient->pivot->qty_required;
                $meal->ingredients()->decrement('quantity', $value); 
                return redirect()->back()->with('message', 'Enjoy your Meal! Updated your inventory!');
            } else {
                return redirect()->back()->with('message', 'Not enough Ingredients');
            }
            }
    }

    public function mealSuggestions() {

            $data['meals'] = Meal::whereHas('ingredients', fn($query) => 
            $query->whereColumn('quantity', '>=', 'qty_required')
            )->get();
            return view('meals.render', $data);
    }
}