<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Models\Meal;
use App\Models\Ingredient;

class CategoryController extends Controller
{
 
    public function index(Request $request)
    {
        if(empty($request->input('category_ids'))) {
            $data['ingredients'] = Ingredient::all();
        }  else {
            $category_ids = collect(explode(',', $request->category_ids))
            ->map(fn($i) => trim($i))
            ->all();
            $data['ingredients'] = Ingredient::whereHas('categories', fn($query) => 
            $query->whereIn('categories.id', $category_ids)
            )->get();
                }
                
        return view('ingredients.render', $data);
    }
}
