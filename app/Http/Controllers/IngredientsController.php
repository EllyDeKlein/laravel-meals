<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ingredient;
use App\Models\Category;
use App\Models\Meal;

class IngredientsController extends Controller
{
  
    public function index()
    {
      
        $data['categories'] = Category::all(); 
        $data['ingredients'] = Ingredient::all();
        $data['meals'] = $this->mealSuggestions();

        return view('ingredients.ingredients', $data);
    }

   
    public function create()
    {
        $data['categories'] = Category::all();
         
        return view('ingredients.create', $data);
    }

  
    public function store(Request $request)
    {
        $ingredient = Ingredient::create($this->validateIngredient());
        $ingredient->categories()->attach(request('categories'));
        $ingredient->save();

        return redirect(route('meals.ingredients'));
    }

    public function edit(Ingredient $ingredients)
    {
        $data['categories'] = Category::all();
        $data['ingredients']= $ingredients; 

        return view('ingredients.edit', $data);
    }


    public function update( Request $request, Ingredient $ingredients) 
    {
        $ingredients->update($this->validateIngredient());

        return redirect(route('meals.ingredients'));
    }

  
    public function destroy(Ingredient $ingredients)
    {
        $ingredients->delete();

        return redirect(route('meals.ingredients'));
    }

    public function validateIngredient() {
        return request()->validate([
            'name' => ['required', 'min:2'],
            'quantity' => ['required', 'digits_between:1,500']
        ]);
    }

    public function mealSuggestions() {

        return Meal::whereHas('ingredients', fn($query) => 
            $query->whereColumn('quantity', '>=', 'qty_required')
            )->get();
    }
}
