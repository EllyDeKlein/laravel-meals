<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    use HasFactory;
    
    protected $fillable = ['name','quantity','categories_id','qty_required'];

    public function meals() {
        return $this->belongsToMany(Meal::class, 'ingredients_meals');
    }

    public function categories() {
        return $this->belongsToMany(Category::class, 'ingredients_category');
}


    public function scopeWithFilters($query)
    {
        return $query->when(count(request()->input('categories', [])), function ($query) {
            $query->whereIn('categories_id', request()->input('categories'));

        });
    }
}
