<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'counter',
        'recipe',
    ];

    public function ingredients() {
        return $this->belongsToMany(Ingredient::class, 'ingredients_meals')
        ->withPivot(['qty_required']);
}
}