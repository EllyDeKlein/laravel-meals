<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IngredientsMealRel extends Model
{
    use HasFactory;
    protected $fillable = [
        'meal_id',
        'ingredient_id',
        'qty_required',
        'categories_id'
    ];
    protected $table = 'ingredients_meals';

    public function meal(){
        return parent::belongsTo(Meal::class);
    }

    public function ingredient(){
        return parent::belongsTo(Ingredient::class);
    }


}
