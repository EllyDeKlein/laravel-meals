<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientsCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredients_category', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('category_id'); 
            $table->unsignedBigInteger('ingredient_id'); 


            $table->foreign('ingredient_id') 
            ->references('id')
            ->on('ingredients')
            ->onDelete('cascade');

            
            $table->foreign('category_id') //Hij ziet hier dus van userS user..
            ->references('id')
            ->on('categories')
            ->onDelete('cascade');

            $table->unique(['category_id', 'ingredient_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredients_category');
    }
}
