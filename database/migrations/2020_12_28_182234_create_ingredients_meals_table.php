<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientsMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredients_meals', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('ingredient_id'); 
            $table->unsignedBigInteger('meal_id');
            $table->integer('qty_required')->nullable()->default(0);
            

            $table->foreign('ingredient_id') 
            ->references('id')
            ->on('ingredients')
            ->onDelete('cascade');

            $table->foreign('meal_id')
            ->references('id')
            ->on('meals')
            ->onDelete('cascade');

            $table->unique(['ingredient_id', 'meal_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredients_meals');
    }
}
