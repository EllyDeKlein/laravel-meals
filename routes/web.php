<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IngredientsController;
//Home
Route::get('/',                                             'App\Http\Controllers\MealsController@index')
    ->name('meals.home');
    
//Ingredients
Route::get('/ingredients',                                  'App\Http\Controllers\IngredientsController@index')
    ->name('meals.ingredients');

Route::get('/ingredients/{ingredients}/edit',               'App\Http\Controllers\IngredientsController@edit')
    ->name('ingredients.edit');

Route::match(['put', 'patch'],'/ingredients/{ingredients}', 'App\Http\Controllers\IngredientsController@update')
    ->name('ingredients.update');

Route::delete('/ingredients/{ingredients}',                 'App\Http\Controllers\IngredientsController@destroy')
    ->name('ingredients.delete');

Route::get('/ingredients/create',                           'App\Http\Controllers\IngredientsController@create')
    ->name('ingredients.create');
    
Route::post('/ingredients/store',                           'App\Http\Controllers\IngredientsController@store')
    ->name('ingredients.store');
    
Route::get('/ingredients/category',                         'App\Http\Controllers\CategoryController@index')
    ->name('ingredients.category');
//Meals
Route::get('/meals',                                        'App\Http\Controllers\MealsController@create')
    ->name('meals.add');
Route::post('/meals/make',                                  'App\Http\Controllers\MealsController@store')
    ->name('meals.make');
Route::get('/meals/show/{meal}',                            'App\Http\Controllers\MealsController@show')
    ->name('meal.show');
Route::get('/meals/update/{meal}',                          'App\Http\Controllers\MealsController@updateInventory')
    ->name('meal.selected');
Route::delete('/meals/{meals}',                             'App\Http\Controllers\MealsController@destroy')
    ->name('meals.delete');
Route::get('/meals/edit/{id}',                              'App\Http\Controllers\MealsController@edit')
    ->name('meals.edit');
Route::match(['put', 'patch'],'/meals/update/{meals}',      'App\Http\Controllers\MealsController@update')
    ->name('meals.update');
